package com.project.orent.ui.faq;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import com.project.orent.R;

import java.util.Objects;

public class FaqDetailActivity extends AppCompatActivity {
    public static final String EXTRA_QUESTION_TITLE = "EXTRA_QUESTION_TITLE";
    public static final String EXTRA_QUESTION_ANSWER = "EXTRA_QUESTION_ANSWER";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_faq_detail);
        initAppbar();
        TextView textTitle = findViewById(R.id.detail_faq_question_title);
        TextView textAnswer = findViewById(R.id.detail_faq_question_answer);
        Intent intent = getIntent();
        textTitle.setText(intent.getStringExtra(EXTRA_QUESTION_TITLE));
        textAnswer.setText(intent.getStringExtra(EXTRA_QUESTION_ANSWER));
    }

    private void initAppbar() {
        Toolbar toolbar = findViewById(R.id.faq_detail_toolbar);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back);
    }
}
