package com.project.orent.model;

public class MyDriver {
    private String booked_driver_name;
    private String booked_driver_location;
    private String booked_driver_phone;

    public MyDriver() {
    }

    public MyDriver(String booked_driver_name, String booked_driver_location, String booked_driver_phone) {
        this.booked_driver_name = booked_driver_name;
        this.booked_driver_location = booked_driver_location;
        this.booked_driver_phone = booked_driver_phone;
    }

    public String getBooked_driver_name() {
        return booked_driver_name;
    }

    public void setBooked_driver_name(String booked_driver_name) {
        this.booked_driver_name = booked_driver_name;
    }

    public String getBooked_driver_location() {
        return booked_driver_location;
    }

    public void setBooked_driver_location(String booked_driver_location) {
        this.booked_driver_location = booked_driver_location;
    }

    public String getBooked_driver_phone() {
        return booked_driver_phone;
    }

    public void setBooked_driver_phone(String booked_driver_phone) {
        this.booked_driver_phone = booked_driver_phone;
    }
}
