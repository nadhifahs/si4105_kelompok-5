package com.project.orent.ui.search;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.project.orent.R;
import com.project.orent.model.Vehicle;
import com.project.orent.ui.content.vehicles.DetailActivity;

import java.util.Objects;

public class SearchActivity extends AppCompatActivity {
    private FirebaseFirestore database;
    private FirestoreRecyclerAdapter adapter;
    private RecyclerView searchResultRecyclerView;
    private TextInputLayout inputLocation;
    private EditText editLocation;
    private Button searchButton;
    private RadioGroup rgVehicleType;
    private String mVehicleType, mLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        initAppbar();
        initComponent();
        initFirebase();

        rgVehicleType.setOnCheckedChangeListener((group, checkedId) -> {
            if (checkedId == R.id.rb_car) {
                mVehicleType = "car";
            } else if (checkedId == R.id.rb_motorcycle) {
                mVehicleType = "motorcycle";
            }
        });

        searchButton.setOnClickListener(v -> {
            mLocation = editLocation.getText().toString().trim();

            if (mLocation.isEmpty()) {
                inputLocation.setError("Location is requires");
                editLocation.requestFocus();
            }

            populateData(mVehicleType, mLocation);
        });
    }

    private void initComponent() {
        searchResultRecyclerView = findViewById(R.id.search_result_recycler_view);
        inputLocation = findViewById(R.id.search_input_location);
        editLocation = findViewById(R.id.search_location_edit_text);
        searchButton = findViewById(R.id.btn_search);
        rgVehicleType = findViewById(R.id.vehicle_type_radio_group);
    }

    private void initFirebase() {
        database = FirebaseFirestore.getInstance();
    }

    private void initAppbar() {
        Toolbar toolbar = findViewById(R.id.search_toolbar);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back);
    }

    private void populateData(String collectionPath, String location) {
        Query query = database.collection(collectionPath)
                .whereEqualTo("location", location);
        FirestoreRecyclerOptions<Vehicle> response = new FirestoreRecyclerOptions.Builder<Vehicle>()
                .setQuery(query, Vehicle.class)
                .build();
        adapter = new FirestoreRecyclerAdapter<Vehicle, VehicleHolder>(response) {

            @NonNull
            @Override
            public VehicleHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.vehicle_list_item_view, parent, false);
                return new VehicleHolder(view);
            }

            @Override
            protected void onBindViewHolder(@NonNull VehicleHolder holder, int position, @NonNull Vehicle model) {
                holder.textName.setText(model.getName());
                holder.textPrice.setText(model.getPrice());
                holder.textLocation.setText(model.getLocation());
                holder.textCapacity.setText(model.getCapacity());
                Glide.with(Objects.requireNonNull(getApplicationContext()))
                        .load(model.getImage_url())
                        .into(holder.vehicleImage);
                holder.itemView.setOnClickListener(v -> {
                    Intent intent = new Intent(getApplicationContext(), DetailActivity.class);
                    intent.putExtra(DetailActivity.EXTRA_VEHICLE_NAME, model.getName());
                    intent.putExtra(DetailActivity.EXTRA_RECENT_DB_COLLECTION, "car");
                    startActivity(intent);
                });
            }
        };

        adapter.notifyDataSetChanged();
        searchResultRecyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        searchResultRecyclerView.setAdapter(adapter);
        adapter.startListening();
    }

    public static class VehicleHolder extends RecyclerView.ViewHolder {
        TextView textName, textPrice, textLocation, textCapacity;
        ImageView vehicleImage;

        VehicleHolder(@NonNull View itemView) {
            super(itemView);
            textName = itemView.findViewById(R.id.vehicle_item_name);
            textPrice = itemView.findViewById(R.id.vehicle_item_price);
            textLocation = itemView.findViewById(R.id.vehicle_item_location);
            textCapacity = itemView.findViewById(R.id.vehicle_item_capacity);
            vehicleImage = itemView.findViewById(R.id.vehicle_item_image);
        }
    }
}
