package com.project.orent.ui.content.userdata;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.project.orent.R;
import com.project.orent.model.Book;

import java.util.Objects;

public class BookingHistoryActivity extends AppCompatActivity {
    private FirebaseFirestore database;
    private FirebaseAuth auth;
    private FirestoreRecyclerAdapter adapter;
    private RecyclerView bookingRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking_history);
        bookingRecyclerView = findViewById(R.id.booking_history_recycler_view);
        initAppbar();
        initFirebase();
        populateData();
    }

    private void initAppbar() {
        Toolbar toolbar = findViewById(R.id.booking_history_toolbar);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back);
    }

    private void initFirebase() {
        database = FirebaseFirestore.getInstance();
        auth = FirebaseAuth.getInstance();
    }

    private void populateData() {
        Query query = database.collection("booking")
                .whereEqualTo("vehicle_book_user", auth.getUid());
        FirestoreRecyclerOptions<Book> response = new FirestoreRecyclerOptions.Builder<Book>()
                .setQuery(query, Book.class)
                .build();

        adapter = new FirestoreRecyclerAdapter<Book, BookingHolder>(response) {

            @NonNull
            @Override
            public BookingHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.booking_history_item_view, parent, false);
                return new BookingHolder(view);
            }

            @Override
            protected void onBindViewHolder(@NonNull BookingHolder holder, int position, @NonNull Book model) {
                holder.textName.setText(model.getVehicle_name());
                holder.textPrice.setText(model.getVehicle_price());
                holder.textDate.setText(model.getVehicle_date());
            }
        };

        adapter.notifyDataSetChanged();
        bookingRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        bookingRecyclerView.setAdapter(adapter);
    }

    public static class BookingHolder extends RecyclerView.ViewHolder {
        TextView textName, textPrice, textDate;

        BookingHolder(@NonNull View itemView) {
            super(itemView);
            textName = itemView.findViewById(R.id.booking_item_name);
            textPrice = itemView.findViewById(R.id.booking_item_price);
            textDate = itemView.findViewById(R.id.booking_item_date_range);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        adapter.startListening();
    }

    @Override
    protected void onStop() {
        super.onStop();
        adapter.stopListening();
    }
}
