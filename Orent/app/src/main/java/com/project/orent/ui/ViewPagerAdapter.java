package com.project.orent.ui;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.project.orent.ui.content.vehicles.CarFragment;
import com.project.orent.ui.content.driver.DriverFragment;
import com.project.orent.ui.content.vehicles.MotorcycleFragment;

public class ViewPagerAdapter extends FragmentPagerAdapter {
    private Fragment[] childFragment;
    private String[] fragmentPageTitle;

    ViewPagerAdapter(@NonNull FragmentManager fm, int behavior) {
        super(fm, behavior);
        childFragment = new Fragment[]{
                new CarFragment(),
                new MotorcycleFragment(),
                new DriverFragment()
        };
        fragmentPageTitle = new String[]{
                "Car",
                "Motorcycle",
                "Driver"
        };
    }


    @NonNull
    @Override
    public Fragment getItem(int position) {
        return childFragment[position];
    }

    @Override
    public int getCount() {
        return childFragment.length;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return fragmentPageTitle[position];
    }
}
