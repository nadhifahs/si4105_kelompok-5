package com.project.orent.model;

import com.google.firebase.firestore.IgnoreExtraProperties;

@IgnoreExtraProperties
public class Vehicle {
    private String address;
    private String capacity;
    private String description;
    private String image_url;
    private String location;
    private String name;
    private String police_number;
    private String price;
    private String speed;
    private String tank;

    public Vehicle() {
    }

    public Vehicle(String address, String capacity, String description, String image_url, String location, String name, String police_number, String price, String speed, String tank) {
        this.address = address;
        this.capacity = capacity;
        this.description = description;
        this.image_url = image_url;
        this.location = location;
        this.name = name;
        this.police_number = police_number;
        this.price = price;
        this.speed = speed;
        this.tank = tank;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCapacity() {
        return capacity;
    }

    public void setCapacity(String capacity) {
        this.capacity = capacity;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPolice_number() {
        return police_number;
    }

    public void setPolice_number(String police_number) {
        this.police_number = police_number;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getSpeed() {
        return speed;
    }

    public void setSpeed(String speed) {
        this.speed = speed;
    }

    public String getTank() {
        return tank;
    }

    public void setTank(String tank) {
        this.tank = tank;
    }
}
