package com.project.orent.ui;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.tabs.TabLayout;
import com.project.orent.R;
import com.project.orent.ui.auth.ProfileSettingActivity;
import com.project.orent.ui.content.driver.MyDriverActivity;
import com.project.orent.ui.content.userdata.BookingHistoryActivity;
import com.project.orent.ui.faq.FaqActivity;
import com.project.orent.ui.search.SearchActivity;

import java.util.Objects;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.main_toolbar);
        FloatingActionButton searchFab = findViewById(R.id.main_fab);

        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayShowTitleEnabled(false);

        ViewPager viewPager = findViewById(R.id.main_view_pager);
        viewPager.setAdapter(new ViewPagerAdapter(getSupportFragmentManager(), 1));

        TabLayout tabLayout = findViewById(R.id.main_tab_layout);
        tabLayout.setupWithViewPager(viewPager);

        searchFab.setOnClickListener(v ->
                startActivity(new Intent(getApplicationContext(), SearchActivity.class))
        );
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.profile) {
            startActivity(new Intent(getApplicationContext(), ProfileSettingActivity.class));
        } else if (item.getItemId() == R.id.my_booking) {
            startActivity(new Intent(getApplicationContext(), BookingHistoryActivity.class));
        } else if (item.getItemId() == R.id.my_driver) {
            startActivity(new Intent(getApplicationContext(), MyDriverActivity.class));
        } else if (item.getItemId() == R.id.faq_list) {
            startActivity(new Intent(getApplicationContext(), FaqActivity.class));
        }
        return true;
    }
}