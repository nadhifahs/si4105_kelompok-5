package com.project.orent.model;

public class Driver {
    private String driver_ability;
    private int driver_age;
    private String driver_location;
    private String driver_name;
    private String driver_phone;
    private String driver_photo;

    public Driver() {

    }

    public Driver(String driver_ability, int driver_age, String driver_location, String driver_name, String driver_phone, String driver_photo) {
        this.driver_ability = driver_ability;
        this.driver_age = driver_age;
        this.driver_location = driver_location;
        this.driver_name = driver_name;
        this.driver_phone = driver_phone;
        this.driver_photo = driver_photo;
    }

    public String getDriver_ability() {
        return driver_ability;
    }

    public void setDriver_ability(String driver_ability) {
        this.driver_ability = driver_ability;
    }

    public int getDriver_age() {
        return driver_age;
    }

    public void setDriver_age(int driver_age) {
        this.driver_age = driver_age;
    }

    public String getDriver_location() {
        return driver_location;
    }

    public void setDriver_location(String driver_location) {
        this.driver_location = driver_location;
    }

    public String getDriver_name() {
        return driver_name;
    }

    public void setDriver_name(String driver_name) {
        this.driver_name = driver_name;
    }

    public String getDriver_phone() {
        return driver_phone;
    }

    public void setDriver_phone(String driver_phone) {
        this.driver_phone = driver_phone;
    }

    public String getDriver_photo() {
        return driver_photo;
    }

    public void setDriver_photo(String driver_photo) {
        this.driver_photo = driver_photo;
    }
}
