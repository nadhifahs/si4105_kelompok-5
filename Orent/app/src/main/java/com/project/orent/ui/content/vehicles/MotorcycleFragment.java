package com.project.orent.ui.content.vehicles;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.project.orent.R;
import com.project.orent.model.Vehicle;

import java.util.Objects;

public class MotorcycleFragment extends Fragment {
    private FirebaseFirestore database;
    private FirestoreRecyclerAdapter adapter;
    private RecyclerView motorcycleRecyclerView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_motorcycle, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        motorcycleRecyclerView = view.findViewById(R.id.motorcycle_list_recycler_view);
        initFirebase();
        populateData();
    }

    private void initFirebase() {
        database = FirebaseFirestore.getInstance();
    }

    private void populateData() {
        Query query = database.collection("motorcycle");
        FirestoreRecyclerOptions<Vehicle> response = new FirestoreRecyclerOptions.Builder<Vehicle>()
                .setQuery(query, Vehicle.class)
                .build();
        adapter = new FirestoreRecyclerAdapter<Vehicle, VehicleHolder>(response) {

            @NonNull
            @Override
            public VehicleHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.vehicle_list_item_view, parent, false);
                return new VehicleHolder(view);
            }

            @Override
            protected void onBindViewHolder(@NonNull VehicleHolder holder, int position, @NonNull Vehicle model) {
                holder.textName.setText(model.getName());
                holder.textPrice.setText(model.getPrice());
                holder.textLocation.setText(model.getLocation());
                holder.textCapacity.setText(model.getCapacity());
                Glide.with(Objects.requireNonNull(getActivity()))
                        .load(model.getImage_url())
                        .into(holder.vehicleImage);
                holder.itemView.setOnClickListener(v -> {
                    Intent intent = new Intent(getActivity(), DetailActivity.class);
                    intent.putExtra(DetailActivity.EXTRA_VEHICLE_NAME, model.getName());
                    intent.putExtra(DetailActivity.EXTRA_RECENT_DB_COLLECTION, "motorcycle");
                    startActivity(intent);
                });
            }
        };

        adapter.notifyDataSetChanged();
        motorcycleRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        motorcycleRecyclerView.setAdapter(adapter);
    }

    public static class VehicleHolder extends RecyclerView.ViewHolder {
        TextView textName, textPrice, textLocation, textCapacity;
        ImageView vehicleImage;

        VehicleHolder(@NonNull View itemView) {
            super(itemView);
            textName = itemView.findViewById(R.id.vehicle_item_name);
            textPrice = itemView.findViewById(R.id.vehicle_item_price);
            textLocation = itemView.findViewById(R.id.vehicle_item_location);
            textCapacity = itemView.findViewById(R.id.vehicle_item_capacity);
            vehicleImage = itemView.findViewById(R.id.vehicle_item_image);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        adapter.startListening();
    }

    @Override
    public void onStop() {
        super.onStop();
        adapter.stopListening();
    }
}
