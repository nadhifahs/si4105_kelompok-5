package com.project.orent.ui.auth;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.project.orent.ui.MainActivity;
import com.project.orent.R;

import java.util.Objects;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    private FirebaseAuth auth;
    private String mEmail;
    private String mPassword;
    private TextInputLayout inputEmail;
    private TextInputLayout inputPassword;
    private EditText editEmail;
    private EditText editPassword;
    private Button loginButton;
    private Button newAccountButton;
    private ProgressBar loginLoading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initComponent();
        initFirebaseAuth();
        initButtons();
    }

    private void initComponent() {
        inputEmail = findViewById(R.id.login_input_email);
        inputPassword = findViewById(R.id.login_input_password);
        editEmail = findViewById(R.id.email_edit_text);
        editPassword = findViewById(R.id.password_edit_text);
        loginButton = findViewById(R.id.btn_login);
        newAccountButton = findViewById(R.id.btn_new_account);
        loginLoading = findViewById(R.id.login_loading);
    }

    private void initFirebaseAuth() {
        auth = FirebaseAuth.getInstance();
    }

    private void initDataInput() {
        mEmail = editEmail.getText().toString().trim();
        mPassword = editPassword.getText().toString().trim();

        if (mEmail.isEmpty()) {
            inputEmail.setError("Email is required");
            editEmail.requestFocus();
        }

        if (!Patterns.EMAIL_ADDRESS.matcher(mEmail).matches()) {
            inputEmail.setError("Email is invalid");
            editEmail.requestFocus();
        }

        if (mPassword.isEmpty()) {
            inputPassword.setError("Password is required");
            editPassword.requestFocus();
        }

        if (mPassword.length() < 6) {
            inputPassword.setError("Password must at least 6 character");
            editPassword.requestFocus();
        }
    }

    private void initButtons() {
        loginButton.setOnClickListener(this);
        newAccountButton.setOnClickListener(this);
    }

    private void login() {
        initDataInput();
        loginLoading.setVisibility(View.VISIBLE);
        auth.signInWithEmailAndPassword(mEmail, mPassword)
                .addOnCompleteListener(task -> {
                    loginLoading.setVisibility(View.GONE);
                    if (task.isSuccessful()) {
                        finish();
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                    } else {
                        Toast.makeText(
                                getApplicationContext(),
                                Objects.requireNonNull(task.getException()).getMessage(),
                                Toast.LENGTH_SHORT
                        ).show();
                    }
                });
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btn_login) {
            login();
        } else if (v.getId() == R.id.btn_new_account) {
            finish();
            startActivity(new Intent(this, RegisterActivity.class));
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (auth.getCurrentUser() != null) {
            finish();
            startActivity(new Intent(this, MainActivity.class));
        }
    }
}
