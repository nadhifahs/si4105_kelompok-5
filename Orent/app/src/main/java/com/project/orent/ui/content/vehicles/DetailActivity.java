package com.project.orent.ui.content.vehicles;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.project.orent.R;
import com.project.orent.ui.content.BookingActivity;

import java.util.Objects;

public class DetailActivity extends AppCompatActivity implements View.OnClickListener {
    private ProgressBar detailLoading;
    private ImageView detailVehicleImage;
    private TextView textVehicleName, textVehiclePrice, textVehicleSpeed;
    private TextView textVehicleSeat, textVehicleTank, textVehicleDesc;
    private Button bookNowButton;
    private FirebaseFirestore database;
    public static final String EXTRA_VEHICLE_NAME = "EXTRA_VEHICLE_NAME";
    public static final String EXTRA_RECENT_DB_COLLECTION = "EXTRA_DB_COLLECTION";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        Intent intent = getIntent();

        initAppBar();
        initFireStore();
        initComponent();
        getVehicleData(
                intent.getStringExtra(EXTRA_RECENT_DB_COLLECTION),
                intent.getStringExtra(EXTRA_VEHICLE_NAME)
        );

        bookNowButton.setOnClickListener(this);
    }

    private void initAppBar() {
        Toolbar toolbar = findViewById(R.id.detail_toolbar);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back);
    }

    private void initFireStore() {
        database = FirebaseFirestore.getInstance();
    }

    private void initComponent() {
        detailLoading = findViewById(R.id.detail_loading);
        detailVehicleImage = findViewById(R.id.detail_vehicle_image);
        textVehicleName = findViewById(R.id.detail_vehicle_name);
        textVehiclePrice = findViewById(R.id.detail_vehicle_price);
        textVehicleSpeed = findViewById(R.id.detail_vehicle_speed);
        textVehicleSeat = findViewById(R.id.detail_vehicle_seat);
        textVehicleTank = findViewById(R.id.detail_vehicle_tank);
        textVehicleDesc = findViewById(R.id.detail_vehicle_description);
        bookNowButton = findViewById(R.id.button_book_vehicle);
    }

    private void getVehicleData(String collectionPath, String vehicleName) {
        detailLoading.setVisibility(View.VISIBLE);

        Query query = database.collection(collectionPath)
                .whereEqualTo("name", vehicleName);

        query.get().addOnCompleteListener(task -> {
            detailLoading.setVisibility(View.GONE);
            if (task.isSuccessful()) {
                if (task.getResult() != null) {
                    for (QueryDocumentSnapshot document : task.getResult()) {
                        document.getData();
                        Glide.with(getApplicationContext())
                                .load(document.getData().get("image_url"))
                                .into(detailVehicleImage);
                        textVehicleName
                                .setText(Objects.requireNonNull(document.getData().get("name")).toString());
                        textVehiclePrice
                                .setText(Objects.requireNonNull(document.getData().get("price")).toString());
                        textVehicleSpeed
                                .setText(Objects.requireNonNull(document.getData().get("speed")).toString());
                        textVehicleSeat
                                .setText(Objects.requireNonNull(document.getData().get("capacity")).toString());
                        textVehicleTank
                                .setText(Objects.requireNonNull(document.getData().get("tank")).toString());
                        textVehicleDesc
                                .setText(Objects.requireNonNull(document.getData().get("description")).toString());
                    }
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.button_book_vehicle) {
            Intent intent = new Intent(this, BookingActivity.class);
            String vehicleName = textVehicleName.getText().toString();
            String vehiclePrice = textVehiclePrice.getText().toString();
            intent.putExtra(BookingActivity.EXTRA_VEHICLE_NAME, vehicleName);
            intent.putExtra(BookingActivity.EXTRA_VEHICLE_PRICE, vehiclePrice);
            startActivity(intent);
        }
    }
}
