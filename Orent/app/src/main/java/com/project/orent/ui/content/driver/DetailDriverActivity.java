package com.project.orent.ui.content.driver;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.project.orent.R;
import com.project.orent.ui.content.SuccessActivity;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class DetailDriverActivity extends AppCompatActivity implements View.OnClickListener {
    private ProgressBar detailDriverLoading;
    private ImageView detailDriverPhoto;
    private TextView textDriverName, textDriverAbility, textDriverLocation, textDriverAge, textDriverPhone;
    private Button bookDriverButton;
    public static final String EXTRA_DRIVER_NAME = "EXTRA_DRIVER_NAME";
    private Map<String, Object> myDriver = new HashMap<>();
    private FirebaseFirestore database;
    private FirebaseAuth auth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_driver);
        Intent intent = getIntent();
        initAppbar();
        initFirebase();
        initComponent();
        getDriverData(intent.getStringExtra(EXTRA_DRIVER_NAME));
        bookDriverButton.setOnClickListener(this);
    }

    private void initAppbar() {
        Toolbar toolbar = findViewById(R.id.detail_driver_toolbar);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back);
    }

    private void initFirebase() {
        database = FirebaseFirestore.getInstance();
        auth = FirebaseAuth.getInstance();
    }

    private void initComponent() {
        detailDriverLoading = findViewById(R.id.detail_driver_loading);
        detailDriverPhoto = findViewById(R.id.detail_driver_photo);
        textDriverName = findViewById(R.id.detail_driver_name);
        textDriverAbility = findViewById(R.id.detail_driver_ability);
        textDriverLocation = findViewById(R.id.detail_driver_location);
        textDriverAge = findViewById(R.id.detail_driver_age);
        textDriverPhone = findViewById(R.id.detail_driver_phone);
        bookDriverButton = findViewById(R.id.button_book_driver);
    }

    private void getDriverData(String driverName) {
        detailDriverLoading.setVisibility(View.VISIBLE);

        Query query = database.collection("driver")
                .whereEqualTo("driver_name", driverName);

        query.get().addOnCompleteListener(task -> {
            detailDriverLoading.setVisibility(View.GONE);
            if (task.isSuccessful()) {
                if (task.getResult() != null) {
                    for (QueryDocumentSnapshot document : task.getResult()) {
                        document.getData();
                        Glide.with(getApplicationContext())
                                .load(document.getData().get("driver_photo"))
                                .into(detailDriverPhoto);
                        textDriverName
                                .setText(Objects.requireNonNull(document.getData().get("driver_name")).toString());
                        textDriverAbility
                                .setText(Objects.requireNonNull(document.getData().get("driver_ability")).toString());
                        textDriverLocation
                                .setText(Objects.requireNonNull(document.getData().get("driver_location")).toString());
                        textDriverAge
                                .setText(Objects.requireNonNull(document.getData().get("driver_age")).toString());
                        textDriverPhone
                                .setText(Objects.requireNonNull(document.getData().get("driver_phone")).toString());
                    }
                }
            }
        });
    }

    private void bookDriver() {
        myDriver.put("booked_driver_name", textDriverName.getText());
        myDriver.put("booked_driver_location", textDriverLocation.getText());
        myDriver.put("booked_driver_phone", textDriverPhone.getText());
        myDriver.put("user_id", auth.getUid());

        database.collection("my_driver")
                .add(myDriver)
                .addOnSuccessListener(documentReference -> {
                    finish();
                    Intent intent = new Intent(this, SuccessActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);

                    Toast.makeText(
                            getApplicationContext(),
                            "Booking Successful",
                            Toast.LENGTH_SHORT
                    ).show();
                })
                .addOnFailureListener(e -> Toast.makeText(
                        getApplicationContext(),
                        "Booking FailedS",
                        Toast.LENGTH_SHORT
                ).show());
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.button_book_driver) {
            bookDriver();
        }
    }
}
