package com.project.orent.ui.content.driver;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.project.orent.R;
import com.project.orent.model.MyDriver;

import java.util.Objects;

public class MyDriverActivity extends AppCompatActivity {
    private FirebaseFirestore database;
    private FirebaseAuth auth;
    private FirestoreRecyclerAdapter adapter;
    private RecyclerView myDriverRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_driver);
        myDriverRecyclerView = findViewById(R.id.my_driver_recycler_view);
        initAppbar();
        initFirebase();
        populateData();
    }

    private void initAppbar() {
        Toolbar toolbar = findViewById(R.id.my_driver_toolbar);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back);
    }

    private void initFirebase() {
        database = FirebaseFirestore.getInstance();
        auth = FirebaseAuth.getInstance();
    }

    private void populateData() {
        Query query = database.collection("my_driver")
                .whereEqualTo("user_id", auth.getUid());
        FirestoreRecyclerOptions<MyDriver> response = new FirestoreRecyclerOptions.Builder<MyDriver>()
                .setQuery(query, MyDriver.class)
                .build();

        adapter = new FirestoreRecyclerAdapter<MyDriver, MyDriverHolder>(response) {

            @NonNull
            @Override
            public MyDriverHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.booked_driver_item_view, parent, false);
                return new MyDriverHolder(view);
            }

            @Override
            protected void onBindViewHolder(@NonNull MyDriverHolder holder, int position, @NonNull MyDriver model) {
                holder.textName.setText(model.getBooked_driver_name());
                holder.textLocation.setText(model.getBooked_driver_location());
                holder.textPhone.setText(model.getBooked_driver_phone());
            }
        };

        adapter.notifyDataSetChanged();
        myDriverRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        myDriverRecyclerView.setAdapter(adapter);
    }

    public static class MyDriverHolder extends RecyclerView.ViewHolder {
        TextView textName, textLocation, textPhone;

        MyDriverHolder(@NonNull View itemView) {
            super(itemView);
            textName = itemView.findViewById(R.id.booked_driver_name);
            textLocation = itemView.findViewById(R.id.booked_driver_location);
            textPhone = itemView.findViewById(R.id.booked_driver_phone);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        adapter.startListening();
    }

    @Override
    protected void onStop() {
        super.onStop();
        adapter.stopListening();
    }
}
