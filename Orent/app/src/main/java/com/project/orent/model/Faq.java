package com.project.orent.model;

public class Faq {
    private String question_title;
    private String question_answer;

    public Faq() {

    }

    public Faq(String question_title, String question_answer) {
        this.question_title = question_title;
        this.question_answer = question_answer;
    }

    public String getQuestion_title() {
        return question_title;
    }

    public void setQuestion_title(String question_title) {
        this.question_title = question_title;
    }

    public String getQuestion_answer() {
        return question_answer;
    }

    public void setQuestion_answer(String question_answer) {
        this.question_answer = question_answer;
    }
}
