package com.project.orent.ui.content;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.util.Pair;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.datepicker.MaterialDatePicker;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.project.orent.R;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class BookingActivity extends AppCompatActivity {
    private FirebaseFirestore database;
    private FirebaseAuth auth;
    private Map<String, Object> vehicleData = new HashMap<>();
    private TextView textVehicleName, textVehiclePrice, textVehicleDate;
    private ImageButton buttonDatePicker;
    private Button buttonBook;
    public static final String EXTRA_VEHICLE_NAME = "EXTRA_VEHICLE_NAME";
    public static final String EXTRA_VEHICLE_PRICE = "EXTRA_DB_COLLECTION";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking);
        database = FirebaseFirestore.getInstance();
        auth = FirebaseAuth.getInstance();
        Intent intent = getIntent();
        initAppBar();
        initComponent();
        textVehicleName.setText(intent.getStringExtra(EXTRA_VEHICLE_NAME));
        textVehiclePrice.setText(intent.getStringExtra(EXTRA_VEHICLE_PRICE));

        MaterialDatePicker.Builder<Pair<Long, Long>> builder =
                MaterialDatePicker.Builder.dateRangePicker();
        builder.setTitleText("Select Booking Date");
        final MaterialDatePicker<?> materialDatePicker = builder.build();

        buttonDatePicker.setOnClickListener(v ->
                materialDatePicker.show(getSupportFragmentManager(), "Date Picker")
        );

        materialDatePicker.addOnPositiveButtonClickListener(selection ->
                textVehicleDate.setText(materialDatePicker.getHeaderText())
        );

        buttonBook.setOnClickListener(v -> saveDataToDb());
    }

    private void initAppBar() {
        Toolbar toolbar = findViewById(R.id.booking_toolbar);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back);
    }

    private void initComponent() {
        textVehicleName = findViewById(R.id.item_name_text);
        textVehiclePrice = findViewById(R.id.item_price_text);
        textVehicleDate = findViewById(R.id.item_date_text);
        buttonDatePicker = findViewById(R.id.date_picker_button);
        buttonBook = findViewById(R.id.process_booking_button);
    }

    private void saveDataToDb() {
        vehicleData.put("vehicle_name", textVehicleName.getText());
        vehicleData.put("vehicle_price", textVehiclePrice.getText());
        vehicleData.put("vehicle_date", textVehicleDate.getText());
        vehicleData.put("vehicle_book_user", auth.getUid());

        database.collection("booking")
                .add(vehicleData)
                .addOnSuccessListener(documentReference -> {
                            finish();
                            Intent intent = new Intent(this, SuccessActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);

                            Toast.makeText(
                                    getApplicationContext(),
                                    "Booking Successful",
                                    Toast.LENGTH_SHORT
                            ).show();
                        }
                )
                .addOnFailureListener(e ->
                        Toast.makeText(
                                getApplicationContext(),
                                "Booking FailedS",
                                Toast.LENGTH_SHORT
                        ).show());
    }
}
