package com.project.orent.model;

public class Book {
    private String vehicle_name;
    private String vehicle_price;
    private String vehicle_date;

    public Book() {

    }

    public Book(String vehicle_name, String vehicle_price, String vehicle_date) {
        this.vehicle_name = vehicle_name;
        this.vehicle_price = vehicle_price;
        this.vehicle_date = vehicle_date;
    }

    public String getVehicle_name() {
        return vehicle_name;
    }

    public void setVehicle_name(String vehicle_name) {
        this.vehicle_name = vehicle_name;
    }

    public String getVehicle_price() {
        return vehicle_price;
    }

    public void setVehicle_price(String vehicle_price) {
        this.vehicle_price = vehicle_price;
    }

    public String getVehicle_date() {
        return vehicle_date;
    }

    public void setVehicle_date(String vehicle_date) {
        this.vehicle_date = vehicle_date;
    }
}
