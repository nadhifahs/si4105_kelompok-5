package com.project.orent.ui.auth;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.project.orent.R;

import java.io.IOException;
import java.util.Objects;
import java.util.UUID;

import de.hdodenhof.circleimageview.CircleImageView;

public class ProfileSettingActivity extends AppCompatActivity implements View.OnClickListener {
    private Uri uriProfile;
    private FirebaseFirestore database;
    private FirebaseAuth auth;
    private static final Integer CHOOSE_PROFILE = 300;
    private String photoURL = "";
    private CircleImageView userPhotoImage;
    private TextView textUserName, textUserEmail, textUserAddress, textUserPhone;
    private Button logoutButton, saveProfilePicButton;
    private ProgressBar loadingImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_setting);
        initAppbar();
        initComponent();
        initFirebaseAuth();
        initButtons();
        getUserData();
    }

    @Override
    protected void onStart() {
        super.onStart();
        loadUserInformation();
    }

    private void initAppbar() {
        Toolbar toolbar = findViewById(R.id.profile_toolbar);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back);
    }

    private void initFirebaseAuth() {
        auth = FirebaseAuth.getInstance();
        database = FirebaseFirestore.getInstance();
    }

    private void initComponent() {
        userPhotoImage = findViewById(R.id.user_profile_image);
        logoutButton = findViewById(R.id.btn_logout);
        saveProfilePicButton = findViewById(R.id.btn_save_profile_picture);
        loadingImage = findViewById(R.id.profile_upload_progress);
        textUserName = findViewById(R.id.profile_user_name);
        textUserEmail = findViewById(R.id.profile_user_email);
        textUserAddress = findViewById(R.id.profile_user_address);
        textUserPhone = findViewById(R.id.profile_user_phone);
    }

    private void initButtons() {
        userPhotoImage.setOnClickListener(this);
        logoutButton.setOnClickListener(this);
        saveProfilePicButton.setOnClickListener(this);
    }

    private void loadUserInformation() {
        FirebaseUser user = auth.getCurrentUser();
        if (user != null) {
            if (user.getPhotoUrl() != null) {
                Glide.with(this).load(user.getPhotoUrl().toString()).into(userPhotoImage);
            }
        }
    }

    private void openImageChooser() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(
                Intent.createChooser(intent, "Select profile image"),
                CHOOSE_PROFILE
        );
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.user_profile_image) {
            openImageChooser();
        } else if (v.getId() == R.id.btn_save_profile_picture) {
            saveUserInformation();
        } else if (v.getId() == R.id.btn_logout) {
            auth.signOut();
            finish();
            Intent intent = new Intent(this, LoginActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        assert data != null;
        if (requestCode == CHOOSE_PROFILE && resultCode == RESULT_OK && data.getData() != null) {
            uriProfile = data.getData();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uriProfile);
                userPhotoImage.setImageBitmap(bitmap);
                updateProfileImage();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void updateProfileImage() {
        StorageReference profileRef = FirebaseStorage.getInstance().getReference(
                "profile_pics/" + UUID.randomUUID() + ".jpg"
        );
        loadingImage.setVisibility(View.VISIBLE);
        profileRef.putFile(uriProfile).addOnSuccessListener(taskSnapshot -> {
            loadingImage.setVisibility(View.GONE);
            profileRef.getDownloadUrl().addOnSuccessListener(uri -> photoURL += uri.toString());
            Toast.makeText(getApplicationContext(), "Profile picture updated!", Toast.LENGTH_SHORT).show();
        });
    }

    private void saveUserInformation() {
        FirebaseUser user = auth.getCurrentUser();
        if (user != null) {
            UserProfileChangeRequest profile = new UserProfileChangeRequest.Builder()
                    .setPhotoUri(Uri.parse(photoURL))
                    .build();
            user.updateProfile(profile).addOnCompleteListener(task -> {
                if (task.isSuccessful()) {
                    Toast.makeText(
                            getApplicationContext(),
                            "Profile updated!",
                            Toast.LENGTH_SHORT
                    ).show();
                }
            });
        }
    }

    private void getUserData() {
        Query query = database.collection("users")
                .whereEqualTo("user_id", auth.getUid());

        query.get().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                if (task.getResult() != null) {
                    for (QueryDocumentSnapshot document : task.getResult()) {
                        document.getData();
                        textUserName
                                .setText(Objects.requireNonNull(document.getData().get("user_name")).toString());
                        textUserEmail
                                .setText(Objects.requireNonNull(document.getData().get("user_email")).toString());
                        textUserAddress
                                .setText(Objects.requireNonNull(document.getData().get("user_address")).toString());
                        textUserPhone
                                .setText(Objects.requireNonNull(document.getData().get("user_phone")).toString());
                    }
                }
            }
        });
    }
}
