package com.project.orent.ui.auth;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.firestore.FirebaseFirestore;
import com.project.orent.ui.MainActivity;
import com.project.orent.R;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener {
    private FirebaseAuth auth;
    private FirebaseFirestore database;
    private Map<String, Object> users = new HashMap<>();
    private String mName, mAddress, mPhone, mEmail, mPassword;
    private TextInputLayout inputName, inputAddress, inputPhone, inputEmail, inputPassword;
    private EditText editName, editAddress, editPhone, editEmail, editPassword;
    private Button registerButton;
    private Button recentAccountButton;
    private ProgressBar registerLoading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        initComponent();
        initFirebaseAuth();
        initButtons();
    }

    private void initComponent() {
        inputName = findViewById(R.id.register_input_name);
        inputAddress = findViewById(R.id.register_input_address);
        inputPhone = findViewById(R.id.register_input_phone);
        inputEmail = findViewById(R.id.register_input_email);
        inputPassword = findViewById(R.id.register_input_password);
        editName = findViewById(R.id.name_edit_text);
        editAddress = findViewById(R.id.address_edit_text);
        editPhone = findViewById(R.id.phone_edit_text);
        editEmail = findViewById(R.id.email_edit_text);
        editPassword = findViewById(R.id.password_edit_text);
        registerButton = findViewById(R.id.btn_register);
        recentAccountButton = findViewById(R.id.btn_recent_account);
        registerLoading = findViewById(R.id.register_loading);
    }

    private void initFirebaseAuth() {
        auth = FirebaseAuth.getInstance();
        database = FirebaseFirestore.getInstance();
    }

    private void initDataInput() {
        mName = editName.getText().toString().trim();
        mAddress = editAddress.getText().toString().trim();
        mPhone = editPhone.getText().toString().trim();
        mEmail = editEmail.getText().toString().trim();
        mPassword = editPassword.getText().toString().trim();

        if (mName.isEmpty()) {
            inputName.setError("Name is required");
            editName.requestFocus();
        }

        if (mAddress.isEmpty()) {
            inputAddress.setError("Address is required");
            editAddress.requestFocus();
        }

        if (mPhone.isEmpty()) {
            inputPhone.setError("Phone number is requires");
            editPhone.requestFocus();
        }

        if (mEmail.isEmpty()) {
            inputEmail.setError("Email is required");
            editEmail.requestFocus();
        }

        if (!Patterns.EMAIL_ADDRESS.matcher(mEmail).matches()) {
            inputEmail.setError("Email is invalid");
            editEmail.requestFocus();
        }

        if (mPassword.isEmpty()) {
            inputPassword.setError("Password is required");
            editPassword.requestFocus();
        }

        if (mPassword.length() < 6) {
            inputPassword.setError("Password must at least 6 character");
            editPassword.requestFocus();
        }
    }

    private void initButtons() {
        registerButton.setOnClickListener(this);
        recentAccountButton.setOnClickListener(this);
    }

    private void register() {
        initDataInput();
        registerLoading.setVisibility(View.VISIBLE);
        auth.createUserWithEmailAndPassword(mEmail, mPassword)
                .addOnCompleteListener(task -> {
                    registerLoading.setVisibility(View.GONE);
                    if (task.isSuccessful()) {
                        users.put("user_id", auth.getUid());
                        users.put("user_name", mName);
                        users.put("user_address", mAddress);
                        users.put("user_phone", mPhone);
                        users.put("user_email", mEmail);

                        database.collection("users")
                                .add(users)
                                .addOnSuccessListener(documentReference -> Toast.makeText(
                                        this,
                                        "Database recorded",
                                        Toast.LENGTH_SHORT
                                ).show())
                                .addOnFailureListener(e -> Toast.makeText(
                                        this,
                                        "Database fail",
                                        Toast.LENGTH_SHORT
                                ).show());
                        finish();
                        Toast.makeText(
                                this,
                                "User registered successful",
                                Toast.LENGTH_SHORT
                        ).show();
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                    } else {
                        if (task.getException() instanceof FirebaseAuthUserCollisionException) {
                            Toast.makeText(
                                    this,
                                    "User already registered",
                                    Toast.LENGTH_SHORT
                            ).show();
                        } else {
                            Toast.makeText(
                                    this,
                                    Objects.requireNonNull(task.getException()).getMessage(),
                                    Toast.LENGTH_SHORT
                            ).show();
                        }
                    }
                });
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btn_register) {
            register();
        } else if (v.getId() == R.id.btn_recent_account) {
            finish();
            startActivity(new Intent(this, LoginActivity.class));
        }
    }
}
