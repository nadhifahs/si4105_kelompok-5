package com.project.orent.ui.content.driver;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.project.orent.R;
import com.project.orent.model.Driver;

import java.util.Objects;

public class DriverFragment extends Fragment {
    private FirebaseFirestore database;
    private FirestoreRecyclerAdapter adapter;
    private RecyclerView driverRecyclerView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_driver, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        driverRecyclerView = view.findViewById(R.id.driver_list_recycler_view);
        initFirebase();
        populateData();
    }

    private void initFirebase() {
        database = FirebaseFirestore.getInstance();
    }

    private void populateData() {
        Query query = database.collection("driver");
        FirestoreRecyclerOptions<Driver> response = new FirestoreRecyclerOptions.Builder<Driver>()
                .setQuery(query, Driver.class)
                .build();
        adapter = new FirestoreRecyclerAdapter<Driver, DriverHolder>(response) {

            @NonNull
            @Override
            public DriverHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.driver_item_view, parent, false);
                return new DriverHolder(view);
            }

            @Override
            protected void onBindViewHolder(@NonNull DriverHolder holder, int position, @NonNull Driver model) {
                holder.textName.setText(model.getDriver_name());
                holder.textAbility.setText(model.getDriver_ability());
                holder.textLocation.setText(model.getDriver_location());
                holder.textPhone.setText(model.getDriver_phone());
                Glide.with(Objects.requireNonNull(getActivity()))
                        .load(model.getDriver_photo()).into(holder.driverPhoto);
                holder.itemView.setOnClickListener(v -> {
                    Intent intent = new Intent(getActivity(), DetailDriverActivity.class);
                    intent.putExtra(DetailDriverActivity.EXTRA_DRIVER_NAME, model.getDriver_name());
                    startActivity(intent);
                });
            }
        };

        adapter.notifyDataSetChanged();
        driverRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        driverRecyclerView.setAdapter(adapter);
    }

    public static class DriverHolder extends RecyclerView.ViewHolder {
        TextView textName, textAbility, textLocation, textPhone;
        ImageView driverPhoto;

        DriverHolder(@NonNull View itemView) {
            super(itemView);
            textName = itemView.findViewById(R.id.driver_item_name);
            textAbility = itemView.findViewById(R.id.driver_item_ability);
            textLocation = itemView.findViewById(R.id.driver_item_location);
            textPhone = itemView.findViewById(R.id.driver_item_phone);
            driverPhoto = itemView.findViewById(R.id.driver_item_photo);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        adapter.startListening();
    }

    @Override
    public void onStop() {
        super.onStop();
        adapter.stopListening();
    }
}
