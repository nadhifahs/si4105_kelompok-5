package com.project.orent.ui.faq;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.project.orent.R;
import com.project.orent.model.Faq;

import java.util.Objects;

public class FaqActivity extends AppCompatActivity {
    private FirebaseFirestore database;
    private FirestoreRecyclerAdapter adapter;
    private RecyclerView faqRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_faq);
        faqRecyclerView = findViewById(R.id.faq_recycler_view);
        initAppbar();
        initFirebase();
        populateData();
    }

    private void initAppbar() {
        Toolbar toolbar = findViewById(R.id.faq_toolbar);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back);
    }

    private void initFirebase() {
        database = FirebaseFirestore.getInstance();
    }

    private void populateData() {
        Query query = database.collection("faqs");
        FirestoreRecyclerOptions<Faq> response = new FirestoreRecyclerOptions.Builder<Faq>()
                .setQuery(query, Faq.class)
                .build();

        adapter = new FirestoreRecyclerAdapter<Faq, FaqHolder>(response) {

            @NonNull
            @Override
            public FaqHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.faq_item_view, parent, false);
                return new FaqHolder(view);
            }

            @Override
            protected void onBindViewHolder(@NonNull FaqHolder holder, int position, @NonNull Faq model) {
                holder.textTitle.setText(model.getQuestion_title());
                holder.itemView.setOnClickListener(v -> {
                    Intent intent = new Intent(getApplicationContext(), FaqDetailActivity.class);
                    intent.putExtra(FaqDetailActivity.EXTRA_QUESTION_TITLE, model.getQuestion_title());
                    intent.putExtra(FaqDetailActivity.EXTRA_QUESTION_ANSWER, model.getQuestion_answer());
                    startActivity(intent);
                });
            }
        };

        adapter.notifyDataSetChanged();
        faqRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        faqRecyclerView.setAdapter(adapter);
    }

    public static class FaqHolder extends RecyclerView.ViewHolder {
        TextView textTitle;

        FaqHolder(@NonNull View itemView) {
            super(itemView);
            textTitle = itemView.findViewById(R.id.faq_item_title);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        adapter.startListening();
    }

    @Override
    protected void onStop() {
        super.onStop();
        adapter.stopListening();
    }
}
